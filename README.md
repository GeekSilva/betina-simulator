# Bettina's Simulator
Esse aqui é um simples código que mostra algumas funções que tratam de problemas de MATEMÁTICA FINANCEIRA. 

O nome é bem sugestivo e é inspirado em Bettina Rudolph, funcionário da Empiricus que disse ter conseguido alcançar a quantia de UM MILHÃO DE REAIS começando com R$ 1.520,00.

## Disclaimer
_O objetivo aqui não é denegrir a imagem da Bettina e nem da empresa Empiricus. É apenas uma sátira, que tem como objetivo alertar os novos investidores para que não caiam em contos de fada._

## 1. Ferramentas
Sendo assim vamos fazer algumas projeções e ver como a Bettina conseguiria isso.

### 1.1 Cenário 1
No primeiro cenário temos Bettina fazendo um único aporte, e esse aporte rendendo até um milhão de reais. No script esssa projeção é realizada pela função `juros_compostos_taxa` que apenas calcula a taxa necessária baseada na fórmula de Juros Compostos.

### 1.2 Cenário 2
No segundo cenário temos Bettina fazendo aportes mensais. Nesse segundo cenário é possível informar uma `rentabilidade anual` e um valor de aportes mensais. Assim podemos ter uma projeção melhor elaborada.
