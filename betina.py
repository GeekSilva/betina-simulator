#coding=utf8
from functions import juros_compostos_taxa as jct, taxa_anual_para_mensal as tam

"""
Betina's Simulator is a simple script that demonstrates the path followed by Betina from Empiricus.
"""


print("Olá, seja bem vindo ao Betina's Simulator:\nAqui vamos analisar alguns cenários e verificar como podemos chegar a um patrimônio de 1 MILHÃO de REAIS, com aportes de um ESTAGIÁRIO!!!\n")


while True:
    print("-"*50)
    print("""
    Temos dois cenários aqui, vejamos:

    1 - No primeiro cenário temos um único aporte de R$ 1520,00
    2 - No segundo cenário temos aportes sucessivos de R$ 1520,00 (ou maiores)
    """)
    print("-"*50)
    print()
    cenario = int(input())

    if cenario == 1:
        print("""
    Vamos ao primeiro cenário: Bem, aqui temos um único aporte no valor de R$ 1520,00. Vamos então 
    descrobrir qual a taxa necessária para que eu alcance a bagatela de UM MILHÃO em 3 ANOS.

    E para isso vamos utilizar a formula de Juros Compostos: M = C(1+i)^n
    Nosso Montante (M) é: 1.000.000,00
    Nosso período é (n): 3 anos
    Nosso Capital inicial (C): 1.520,00

    Vamos encontrar a taxa anual:
        """)

        print('Calculando..........')

        print(':) Deu certo (:')
        print()
        print('Precisarei apenas de uma taxa anual de %.2f%%'%(jct(1e6, 1520, 3)))
        print("QUEM NUNCA?")
        

    elif cenario == 2:
        print("""
    Que tal me ajudar com algumas informações? Nesse cenário vamos definir uma rentabilidade anual e o valor do aportes

    Vamos ficar milionários em 3 ANOS??
        """)


        print("Me ajude a encontrar a taxa anual: ")
        taxa_anual = float(input())
        taxa_mensal = tam(taxa_anual)

        print("Quer me informar um valor de aporte? [Se não informar será mantido em 1520 reais]")

        try:
            aporte = float(input())
        except ValueError:
            aporte = 1520


        count = 0
        ma = 0

        print("""
        Taxa de rentabilidade anual: %.2f
        Taxa de rentabilidade mensal: %.2f
        Valor dos aportes mensais: %.2f

        """%(taxa_anual, taxa_mensal*100, aporte))

        while count < 36:
            mf = (ma+aporte)*(taxa_mensal+1)
            ma = mf
            print("Após o %d° mês o montante será: %.2f"%((count+1), mf))

            if mf >= 1e6:
                print('Já chegamos em UM MILHÃO!')

                if count < 35:
                    print('E foi antes dos trẽs anos hein?')
                    break

                break

            count += 1
        else:
            print("\nÉ parece que não chegamos ao milhão com aportes de %.2f :/"%(aporte))
    else:
        pass

    print("-"*50)
    print("""
    Deseja continuar? 

    1 - para sim
    qualquer outro - para não
    """)
    print("-"*50)
    print()

    try:
        continuar = int(input())
    except ValueError:
        break


    if continuar != 1:
        break
