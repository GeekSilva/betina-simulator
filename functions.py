#coding=utf8

def juros_compostos_taxa(montante, capital, periodo, percent=True):
    """
        Esssa função encontra a taxa aplicada, mudando a formula de juros compostos.
    """
    i = ( (float(montante) / float(capital)) ** (1/float(periodo)) ) - 1
    if percent:
        return i*100.0
    else:
        return i





def taxa_anual_para_mensal(taxa_anual):
    """
        Converte a taxa anual para taxa mensal
    """
    return (((1+taxa_anual/100)**(1/12)) - 1)


